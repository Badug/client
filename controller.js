'use strict';

angular.module('MoviesAPI', [])
.controller('MovieController',function($scope, $http){

    $scope.contacts =[];
	$scope.movies;
	
	//get all the movies list
	$scope.loadMovies = function(){
		 $scope.search = "";
		//https://api-badug.c9users.io:8081/movie
		var moviesUrl="https://api-badug.c9users.io:8081/movie";
     	$http.get(moviesUrl)
			.success(function(response){
				$scope.contacts = response;  //ajax request to fetch data into $scope.data
			$scope.movies=[];
		});
	}
	
	//Sorting function 
	$scope.sort = function(keyname){
		$scope.sortKey = keyname;   //set the sortKey to the param passed
		$scope.reverse = !$scope.reverse; //if true make it false and vice versa
	}
	
	//load the search field with the name of the movie selected from the list of available movies from  a third party API OMDB API
	$scope.loadSearch = function(contact){
		 $scope.search = contact.title;
		 var url="http://www.omdbapi.com/?t="+$scope.search+"&tomatoes=true&plot=full";// the url to the 3rd party ApI to get more info on the movie 
   		 $http.get(url)
		 	.success(function(response){ 
				$scope.conts = response; 
		  });
		
	}
	
	//without changing the table values but only more information on the right side
	// Load more information on the selected movie from OMDBAPI .. a 3rd party movies API
	$scope.viewMoreInfo= function(contact){
			$scope.searchmore = contact.title;
			var url="http://www.omdbapi.com/?t="+$scope.searchmore+"&tomatoes=true&plot=full";// the url to the 3rd party ApI to get more info on the movie 
   		 $http.get(url)
		 	.success(function(response){ 
				$scope.conts = response; 
			});
			
		//$http.get("moviesJson/more.json").success(function(response){  $scope.conts = response; });
	}
	
	
	
	$scope.loadMovies();
	
});