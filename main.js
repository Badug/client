var mainApp = angular.module("mainApp", ['ngRoute','ngCookies']);

mainApp.config(function($routeProvider) {
    $routeProvider
       .when('/home', {
	  templateUrl: 'home.html',
	  controller: 'MoviesController'
       })
	    .when('/login', {
	  templateUrl: 'login.html',
	  controller: 'MoviesController'
       })
		 .when('/register', {
	  templateUrl: 'register.html',
	  controller: 'MoviesController'
       })
       .otherwise({
	  redirectTo: '/home'
       });
 });

 mainApp.controller('MoviesController', function($scope, $http,$location) {
    
	$scope.contacts =[];
	$scope.movies;
	
	//get all the movies list
	$scope.loadMovies = function(){
		 $scope.search = "";
		//https://api-badug.c9users.io:8081/movie
		var moviesUrl="https://api-badug.c9users.io:8081/movie";
     	$http.get(moviesUrl)
			.success(function(response){
				$scope.contacts = response;  //ajax request to fetch data into $scope.data
			//$scope.movies=[];
			
			
			//load a default movie on the more information page
			var url="http://www.omdbapi.com/?t=The Bourne Identity &tomatoes=true&plot=full";// the url to the 3rd party ApI to get more info on the movie 
   		 $http.get(url)
		 	.success(function(response2){ 
				$scope.conts = response2;
			});
			
			
			
		});
	}
	
	//Sorting function 
	$scope.sort = function(keyname){
		$scope.sortKey = keyname;   //set the sortKey to the param passed
		$scope.reverse = !$scope.reverse; //if true make it false and vice versa
	}
	
	//load the search field with the name of the movie selected from the list of available movies from  a third party API OMDB API
	$scope.loadSearch = function(contact){
		 $scope.search = contact.title;
		 var url="http://www.omdbapi.com/?t="+$scope.search+"&tomatoes=true&plot=full";// the url to the 3rd party ApI to get more info on the movie 
   		 $http.get(url)
		 	.success(function(response){ 
				$scope.conts = response; 
		  });
		
	}
	
	//without changing the table values but only more information on the right side
	// Load more information on the selected movie from OMDBAPI .. a 3rd party movies API
	$scope.viewMoreInfo= function(contact){
			$scope.searchmore = contact.title;
			var url="http://www.omdbapi.com/?t="+$scope.searchmore+"&tomatoes=true&plot=full";// the url to the 3rd party ApI to get more info on the movie 
   		 $http.get(url)
		 	.success(function(response){ 
				$scope.conts = response; 
			});
			
		//$http.get("moviesJson/more.json").success(function(response){  $scope.conts = response; });
	}
	
	
	$scope.loadMovies();
	
	
	
	$scope.login = function (username, password) {
    if ( username === 'admin' && password === '1234') {
        $scope.user = username;
		alert("Welcome");
		  $location.path("/home");
    } else {
        $scope.loginError = "Login Error!! Invalid username/password combination";
    };
  };
	
 });